#!/usr/bin/env python
""" Simple Calculator

"1+2*3+4" -> 11 (String to Integer)

Algorithm is based on:
https://en.wikipedia.org/wiki/Shunting-yard_algorithm#The_algorithm_in_detail

Email Pieter.Oliver.2012@my.bristol.ac.uk with bugs or feature requests.
"""

import re
import unittest

def check_string_validity(string):
    """ Checks the validity of the given string.

    Args:
        string (string)

    Returns:
        bool: True if it only has the allowed characters, False otherwise.
     """
    regex_filter = "^[0-9,+,\-,*,\/]*$"

    match_obj = re.match(regex_filter, string)
    if match_obj:
        return True
    else:
        print("Not a valid string for computation. Only integers, *, /, + and - allowed.")
        return False

def is_number(str):
    """
    Checks if a string is an integer or not.
    """
    try:
        int(str)
        return True
    except ValueError:
        return False

def peek(stack):
    """
    Shows the top of the stack without removing it.
    """
    return stack[-1] if stack else None

def tokeniser(expression):
    """
    Splits the string into separate numbers and operators.
    """
    tokens = re.findall("[+/*-]|\d+", expression)
    return tokens

def greater_precedence(op1, op2):
    """
    Orders the operators by order of operations.
    """
    order = {'+' : 0, '-' : 0, '*' : 1, '/' : 1}
    return order[op1] > order[op2]

def shunting_yard(tokens):
    """
    Converts from Infix Tokens to Reverse Polish Notation.
    E.g. [1, +, 2, *, 3, -, 4] -> 
         [1, 2, 3, *, +, 4, -]

    """
    operators = []
    queue = []
    for token in tokens:
        if is_number(token):
            queue.append(token)
        else:
            # check the top for a comparison to get the order correct.
            # TODO fix this
            top = peek(operators)
            # if top is None and greater_precedence(top, token):
                # operators.pop(token)
            operators.append(token)

    for token in range(len(operators)):
        # print(operators.pop())
        queue.append(operators.pop())

    return queue

def rpn_calculator(rpn):
    """ Not Implemented
    Takes in sorted rpn stack and returns a single value.
    E.g. ["1", "2", "3", "*", "+", "4", "-"] -> 3
    """
    return rpn


def calculate(expression):
    if check_string_validity(expression):
        tokens = tokeniser(expression)
        rpn = shunting_yard(tokens)
        return rpn_calculator(rpn)
    else:
        return None

def calculate_hack(expression):
    """
    Calculates the string to integer using `eval()` after validation.
    """
    if check_string_validity(expression):
        return eval(expression)
    else:
        return None

# # Example for debugging
# print(calculate("1+2*3"))

class CalculationTestCase(unittest.TestCase):
    @unittest.skip
    def test_specification_example(self):
        """ Integration tests of the calculate function """
        self.assertEqual(calculate("1+2*3+4"), 11)
        self.assertEqual(calculate("1+2*3-4"), 3)

class StringFilterTestCase(unittest.TestCase):
    """ Ensures the filter passes only calculation strings. """
    def test_several_calculation_strings(self):
        """ Examples of actual calculations """
        self.assertTrue(check_string_validity("1+2*3/4-12"))
        self.assertTrue(check_string_validity("55-54"))
    def test_filter_inputs(self):
        """ Tests several strings that should fail. """
        self.assertFalse(check_string_validity("hello calculator"), msg="filter lets through text.")
        self.assertFalse(check_string_validity("eval(print('hello'))"), msg="filter lets some code")
    @unittest.skip
    def test_semicorrect_strings(self):
        """ Other strings that pass but should be blocked. """
        self.assertFalse(check_string_validity("1*-++++3"),
                         msg="shouldn't work with multiple symbols like that")

class PostfixConversionTestCase(unittest.TestCase):
    """ Written to test the conversion. """
    @unittest.skip
    def test_infix_to_postfix(self):
        """ Integration tests of the calculate function """
        self.assertEqual(shunting_yard(["1", "+", "2", "*", "3", "-", "4"]),
                         ["1", "2", "3", "*", "+", "4", "-"])
        self.assertEqual(calculate("1+2*3-4"), 3)
