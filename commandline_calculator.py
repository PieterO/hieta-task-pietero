#!/usr/bin/env python
""" Simple Calculator Commandline Interface
The command line interface is exported to this file to separate concerns.
"""
# commandline_calculator.py

import argparse
from calculator import calculate, calculate_hack

parser = argparse.ArgumentParser(description='Simple Calculator')
parser.add_argument("-c","--calculate", type=str,
                     help="calculates using script, shunting and rpn evaluation. Not implemented yet.")
parser.add_argument("-ch","--calculatehack", type=str,
                     help="calculates using eval, not the best way of doing it.")
args = parser.parse_args()
print(calculate_hack(args.calculatehack))