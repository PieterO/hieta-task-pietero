# Original Specification

You will be creating a piece of software to take a string such as “1+2x3+4” and work out the result.
The software only needs to deal with the operators +, -, ÷ and ×.
The string will only contain operators and numbers.
The software should calculate the result itself and not rely on any libraries. 
Please create basic flow diagrams to show how the software will work. 
Please also include instructions on what language to use, how to manage the source code and software release process.

The software will be used by people across the business.
Please submit your completed task, in PDF format, to hugh.smithson@hieta.biz by 07:30 on Tuesday 31st October.

## Specification analysis

Turn "1+2x3+4" -> 11 (String to Integer)
Check the string is valid for computation?
Don't use any external libraries.
Create flow diagrams.
Document, language, source code management and software release.
Consider deployment to non-technical staff.