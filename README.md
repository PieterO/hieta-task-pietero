# String Calculator

Simple software that computes from a string of numbers and operators a result.

E.g. “1+2x3+4” -> 11

This task was completed for Pieter Oliver's application to Hieta.

## Usage

This can be run using the command line interface (after installation.)

` python commandline_calculator.py -ch "12+9*3-3" ` 


## Getting Started

### Prerequisites for Development

1. Install [Python 3.6.X](https://www.python.org/downloads/release/python-363/) and [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
2. Checkout the latest master version from the remote.
   ```
   git clone https://bitbucket.org/PieterO/hieta-task-pietero
   ```

3. Try several commands
   ```
   python commandline_calculator.py -ch "12+9*3-3"
   ```
 
Make sure to have a linter enabled too, [Pylint](https://www.pylint.org/) helps maintain cleaner code.

### Building an executable

To build an executable:

``` pyinstaller commandline_calculator.py ```

### Using as a library

To import the code to use in another project:
``` 
from calculator import calculate
```
will import the main calculation function.


## Commands for Testing

The tests are run by:

` python -m unittest calculator.py `

Tests that fail are skipped.


# License

This project is licensed under the MIT License.